import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WebserviceService } from '../Services/webservice.service'
import { DataServiceService } from '../Services/data-service.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  listaFilmes;
  constructor(private http: WebserviceService, private dataService: DataServiceService) { }

  ngOnInit() {
  }

  ProcurarFilme(query)
  {   
    this.http.GetFilmesPredicate(query.value).subscribe((data: any) => {
      this.listaFilmes = data.results;
      this.AtualizarListaFilmes();
    });
  }

  AtualizarListaFilmes()
  {
    this.dataService.atualizarLista(this.listaFilmes);
  }
}
