import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmesSaComponent } from './filmes-sa.component';

describe('FilmesSaComponent', () => {
  let component: FilmesSaComponent;
  let fixture: ComponentFixture<FilmesSaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmesSaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmesSaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
