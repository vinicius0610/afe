import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filmes-sa',
  templateUrl: './filmes-sa.component.html',
  styleUrls: ['./filmes-sa.component.css']
})
export class FilmesSaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  public clickedEvent: Event;

  childEventClicked(event: Event){
    this.clickedEvent = event;
  }
}
