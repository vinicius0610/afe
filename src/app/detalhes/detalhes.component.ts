import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WebserviceService } from '../Services/webservice.service';
import { FilmesGridComponent } from '../filmes-grid/filmes-grid/filmes-grid.component';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.css']
})
export class DetalhesComponent implements OnInit {

  id;
  detalhesFilme;

  constructor(private route: ActivatedRoute, private http: WebserviceService ) { 
    this.route.params.subscribe(res => {
      this.id = res.id;
    })
  }

  ngOnInit() {
    this.http.GetFilmeID(this.id).subscribe((data: any) => {
      this.detalhesFilme = data;
    })
  }

}
