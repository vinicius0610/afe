import { Component, OnInit, Input } from '@angular/core';
import { WebserviceService } from 'src/app/Services/webservice.service';
import { DataServiceService } from 'src/app/Services/data-service.service';

@Component({
  selector: 'app-filmes-grid',
  templateUrl: './filmes-grid.component.html',
  styleUrls: ['./filmes-grid.component.css']
})
export class FilmesGridComponent implements OnInit {
  listaFilme;
  existe;
  @Input() event: Event; 

  constructor(private webService: WebserviceService, private dataService: DataServiceService) {
    this.webService.GetFilmes().subscribe((data: any) => {
      this.listaFilme = data.results;
      this.existeFilme();
    })
   }

  ngOnInit() {
    this.dataService.listaAtualizada.subscribe((data: any) => {
      this.listaFilme = data;
      this.existeFilme();
    })
  }

  existeFilme() {
    this.listaFilme.length > 0 ? this.existe = true : this.existe = false;
  }
}
