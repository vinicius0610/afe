import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmesGridComponent } from './filmes-grid.component';

describe('FilmesGridComponent', () => {
  let component: FilmesGridComponent;
  let fixture: ComponentFixture<FilmesGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmesGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmesGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
