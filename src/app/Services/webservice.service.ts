import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const url:string = 'https://api.themoviedb.org/3';
const token:string ='api_key=b894b3b31986a18a668244e92cee7634';

@Injectable({
  providedIn: 'root'
})
export class WebserviceService {

  constructor(private http: HttpClient) {

   }
  
    public GetFilmes() {
      let urlinit = `${url}/movie/popular/?${token}&language=pt-br`;  
      return this.http.get(urlinit);
   }

   public GetFilmesPredicate(predicate){
      let urlinit = `${url}/search/movie?query=${predicate}&${token}&language=pt-br`;  
      return this.http.get(urlinit);
   }

   public GetFilmeID(id){
      let urlinit = `${url}/movie/${id}?${token}&language=pt-br`;
      return this.http.get(urlinit);
   }
}
