import { Injectable, EventEmitter, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WebserviceService } from './webservice.service';

@Injectable({
  providedIn: 'root'
})

export class DataServiceService {


  @Output() listaAtualizada: EventEmitter<any> = new EventEmitter();

  constructor(private webService: WebserviceService) { }

  atualizarLista(lista) {
    this.listaAtualizada.emit(lista)
  }

}
