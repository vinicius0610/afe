import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FilmesGridComponent } from './filmes-grid/filmes-grid/filmes-grid.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { DetalhesComponent } from './detalhes/detalhes.component';
import { Routes, RouterModule } from '@angular/router';
import { FilmesSaComponent } from './filmes-sa/filmes-sa.component';

const appRoutes: Routes = [
  { path: 'home', component: FilmesSaComponent },
  { path: 'detalhes-filme/:id', component: DetalhesComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    FilmesGridComponent,
    SearchBarComponent,
    DetalhesComponent,
    FilmesSaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ) 
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
